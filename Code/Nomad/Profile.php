<?php

namespace Code\Nomad;

use Code\Lib\BaseObject;

class Profile extends BaseObject
{
    public $encoding;
    public $description;
    public $birthday;
    public $next_birthday;
    public $age;
    public $gender;
    public $marital;
    public $sexual;
    public $locale;
    public $region;
    public $postcode;
    public $country;
    public $about;
    public $homepage;
    public $hometown;
    public $keywords;
    public $pronouns;

    /**
     * @return mixed
     */
    public function getEncoding()
    {
        return $this->encoding;
    }

    /**
     * @param mixed $encoding
     * @return Profile
     */
    public function setEncoding($encoding)
    {
        $this->encoding = $encoding;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPronouns()
    {
        return $this->pronouns;
    }

    /**
     * @param mixed $pronouns
     * @return Profile
     */
    public function setPronouns($pronouns)
    {
        $this->pronouns = $pronouns;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Profile
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     * @return Profile
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNextBirthday()
    {
        return $this->next_birthday;
    }

    /**
     * @param mixed $next_birthday
     * @return Profile
     */
    public function setNextBirthday($next_birthday)
    {
        $this->next_birthday = $next_birthday;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     * @return Profile
     */
    public function setAge($age)
    {
        $this->age = $age;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     * @return Profile
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarital()
    {
        return $this->marital;
    }

    /**
     * @param mixed $marital
     * @return Profile
     */
    public function setMarital($marital)
    {
        $this->marital = $marital;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSexual()
    {
        return $this->sexual;
    }

    /**
     * @param mixed $sexual
     * @return Profile
     */
    public function setSexual($sexual)
    {
        $this->sexual = $sexual;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     * @return Profile
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     * @return Profile
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param mixed $postcode
     * @return Profile
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     * @return Profile
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * @param mixed $about
     * @return Profile
     */
    public function setAbout($about)
    {
        $this->about = $about;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * @param mixed $homepage
     * @return Profile
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHometown()
    {
        return $this->hometown;
    }

    /**
     * @param mixed $hometown
     * @return Profile
     */
    public function setHometown($hometown)
    {
        $this->hometown = $hometown;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param mixed $keywords
     * @return Profile
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }
}
