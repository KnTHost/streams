<?php

namespace Code\ActivityStreams;

class PublicKey extends ASObject
{
    public $owner;
    public $signatureAlgorithm;
    public $publicKeyPem;

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     * @return PublicKey
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }


}
