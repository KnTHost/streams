App Customization
=================

A number of possibilities are present to customize the default apps that are presented on your site. The apps are typically defined in a file named something.apd, where something can be an arbitrary name, but usually describes the app in some manner.

Most sites run with files under version control (such as 'git'), so modifying these files is discouraged. It will cause merge conflicts next time your site updates.

But we have a way around this. The first thing you should do is create a new directory inside this directory, with a name of 'site'. Copy any files you wish to change into the site directory, and from there you can modify them without causing conflict.

You can also change the default order of the apps either on the navigation bar at the top of the page or in the apps pull-down "hamburger" menu. All you can do here is create a default or initial layout. Your site members can change the layout as they wish, and once they've changed it, you've lost control.

In order for an app to appear in the navigation bar, it needs to have 'nav_pinned_app' as one of its categories. In order for it to appear in the hamburger (apps) menu, it needs to have 'nav_featured_app' as one of its categories.

Many of the system apps already contain the 'nav_featured_app' category and will show up automatically in the hamburger menu. Few if any contain the 'nav_pinned_app' category, so if you want something to appear on the navigation bar, you will need to copy that .apd file to the site directory and edit 'categories:'. This is a commas separated list of available categories and may not exist, or may exist and also contain other categories.

Also, in order for a system app to be loaded into the system, it must have changed. There is a version: field in the app which lets you control this. It doesn't matter what the version is, only that if you want your changes to work, the version needs to change. The developers usually use whole numbers for the version, so to avoid conflict with them, it is recommended you use an extra decimal place such as 2.1.

The ordering of apps is provided in files you create with the filenames
app/site/nav_order (for the navigation bar), and app/site/app_order (for the hamburger menu). These contain the exact name of the app as shown in the name: field of the .apd file, one per line.

With this background out of the way, let's do an example. You want the Connections and Stream apps to appear on the top navigation bar for your site members.

First create the site directory

```
cd app
mkdir site
```

Copy the connections.apd and stream.apd files to this directory

```
cp connections.apd site
cp stream.apd site
```

Edit both of these files, changing the version and categories.

For the site/connections.apd file the result will look like this:

```
version: 1.1
url: $baseurl/connections
requires: local_channel
name: Connections
photo: icon:users
categories: nav_featured_app, nav_pinned_app, Networking
```

For the site/stream.apd file the result will look like this:

```
version: 2.1
url: $baseurl/stream
requires: local_channel
name: Stream
photo: icon:list-alt
categories: nav_featured_app, nav_pinned_app, Networking
```

Now create a file called site/nav_order and add the entries (using the name: attribute). It will look like this:

```
Stream
Connections
```

Assuming you haven't changed the default app order, if you reload the page these apps should appear in the navigation bar.

If it doesn't, visit https://[yoursite]/pconfig/system

and look for pconfig[n][system][import_system_apps] and click it. 'n' will be a number, your channel_id. This contains a date, most likely today's date. Set it to yesterday's date and submit. Then it should display.

Changing the app menu should be much easier, if you just want to change the order of existing apps, because usually you won't need to change the categories or versions. You just want to create a file called site/app_order listing the entries in the order you desire. All of the installed system apps are listed in a file called 'default_apps' in the app directory. You can order these the way you wish. Not all of the entries in this file have the 'nav_featured_app' category, but the important ones do. If you want one of those which doesn't have this category to appear in the app hamburger menu, copy it to the site directory and edit it just like the examples above for the navigation menu.

You can also just list one or two entries you want at the top of the list and the software will use the system ordering (alphabetic) for the remainder. If the developer release new system apps they want to appear in this menu, these will appear below any ordered entries you specify.
